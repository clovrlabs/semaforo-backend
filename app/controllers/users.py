# User management routes
#
# Copyright (C) 2020 Clovrlabs SL
#
# This program is free software: you can redistribute it
# and/or modify it under the terms of the GNU General Public License as published
# by the Free Software Foundation, either version 3 of the License, or (at your
# option) any later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
# PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# this program. If not, see https://www.gnu.org/licenses/.

from datetime import datetime

from flask import current_app, request, g
from flask_json import json_response, JsonError

from ..core import gsuite, admin_required, http, get_email_domain, send_email
from ..models import db, User, Organization

import logging

event_logger = logging.getLogger("event_logger")


@admin_required
def get_users():
    items = []
    users = User.query.filter_by(organization_id=g.user.organization_id)
    for user in users:
        items.append(
            {
                "id": user.id,
                "admin": user.admin,
                "google_admin": user.google_admin,
                "name": user.name,
                "email": user.email,
                "email_sent": user.email_sent,
                "last_api_access": user.last_api_access,
            }
        )
    return json_response(items=items)


@admin_required
def update_users():
    items = []
    data = request.get_json(force=True, silent=True, cache=True)
    if data is None:
        return http.error(http.MISSING_DATA)
    if "users" not in data or not isinstance(data["user"], list):
        raise http.error(http.BAD_FORMAT)
    new_count = seen_count = 0
    for user_data in data["users"]:
        user_obj = User.query.filter_by(
            email=user_data["email"], organization_id=g.user.organization_id
        ).one_or_none()
        if user_obj is None:
            # the user is a new one, create it
            user_obj = User.from_json(user_data)
            new_count += 1
        else:
            # update the user fields from the current one
            for key, value in user_data.items():
                if hasattr(user_obj, key):
                    setattr(user_obj, key, value)
            seen_count += 1
        db.session.add(user_obj)
    db.session.commit()
    status = "added {} new users, updated {} known users".format(new_count, seen_count)
    event_logger.info(status)
    return json_response(status=status)


@admin_required
def get_user(user_id):
    user = User.query.filter_by(
        id=user_id, organization_id=g.user.organization_id
    ).first()
    if user is None:
        return http.error(http.RECORD_NOT_FOUND)

    return json_response(
        item={
            "id": user_id,
            "admin": user.admin,
            "google_admin": user.google_admin,
            "name": user.name,
            "email": user.email,
            "email_sent": user.email_sent,
            "last_api_access": user.last_api_access,
        }
    )


@admin_required
def import_user():
    """this route is used from the frontend to trigger the importing of users
    in the backend. it requires to be semaforo admin and, due to the way
    user delegation works in google, also google admin for the current domain"""
    google_users = gsuite.get_users(g.access_token, domain=g.domain)
    db_users = User.query.all()
    added = deleted = updated = 0
    # first delete all users that are not in the current google users list
    all_google_emails = [x.get("primaryEmail") for x in google_users["users"]]
    for db_user in db_users:
        # identify them by id
        if db_user.email not in all_google_emails:
            db.session.delete(db_user)
            deleted += 1

    # iterate the current google user list and import them
    for google_user in google_users["users"]:
        google_email = google_user.get("primaryEmail")
        google_id = google_user.get("id")
        # look them up by id first and email second
        known_user = User.query.filter_by(google_id=google_id).one_or_none()
        if known_user is None:
            known_user = User.query.filter_by(email=google_email).one_or_none()

        this_email = google_user.get("primaryEmail")
        if this_email is None:
            for cur in google_user.get("emails"):
                if "primary" in cur and cur["primary"] is True:
                    this_email = cur["address"]
                    break

        if known_user is None:
            # new user, add it to the db
            known_user = User()
            # if the user is admin in google, make admin in semaforo
            known_user.admin = google_user.get("isAdmin", False)
            this_domain = get_email_domain(this_email)
            org = Organization.query.filter_by(domain=this_domain).one_or_none()
            if org is None:
                org = Organization()
                org.domain = domain
                db.session.add(org)
                db.session.commit()
            known_user.organization = org
            added += 1
        else:
            updated += 1
        # update the known values
        known_user.google_admin = google_user.get("isAdmin", False)
        known_user.google_id = google_user.get("id")
        known_user.email = this_email
        known_user.name = google_user.get("name", {}).get("fullName", "undefined")
        db.session.add(known_user)
    # commmit all users
    db.session.commit()
    event_logger.info(
        f"imported users: added {added}, deleted {deleted}, updated {updated}"
    )
    return json_response()


@admin_required
def invite_user(user_id):
    args = request.json
    subject = args.get("subject", "").strip()
    message = args.get("message", "").strip()
    if not subject or not message:
        return http.error(http.BAD_FORMAT)
    config = current_app.config

    user = User.query.get(user_id)
    if not user:
        return http.error(http.RECORD_NOT_FOUND)

    try:
        send_email(
            config["SMTP_FROM"],
            user.email,
            subject,
            message,
            config["SMTP_USERNAME"],
            config["SMTP_PASSWORD"],
        )
    except Exception as e:
        return http.error(http.INTERNAL_SERVER_ERROR, str(e))

    # we have successfully sent the email if we got to this point,
    # update the user `email_sent` field
    user.email_sent = datetime.now()
    db.session.add(user)
    db.session.commit()
    event_logger.info("user {} invited".format(user.name))
    return json_response(id=user.id)


@admin_required
def delete_user(user_id):
    if g.user.id == user_id:
        return http.error(http.FORBIDDEN, "Please don't kill yourself")

    user = User.query.filter_by(
        id=user_id, organization_id=g.user.organization_id
    ).first()
    if user is None:
        return http.error(http.RECORD_NOT_FOUND)
    user_name = user.name
    user_id = user.id
    db.session.delete(user)
    db.session.commit()

    event_logger.info("user {} ({}) deleted".format(user_name, user_id))
    return json_response()
