# Email handling and validation functions
#
# Copyright (C) 2020 Clovrlabs SL
#
# This program is free software: you can redistribute it
# and/or modify it under the terms of the GNU General Public License as published
# by the Free Software Foundation, either version 3 of the License, or (at your
# option) any later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
# PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# this program. If not, see https://www.gnu.org/licenses/.

import requests
import logging
import sys
from ..models import db, User, Organization
from flask import current_app, g
from flask_json import json_response
import json

logging.basicConfig(stream=sys.stdout)


class GophishAPI(object):
    
    def __init__(self):
            self.url='https://localhost:3333/api/' #current_app.config["GOPHISH_URL"]
            self.api_key='b971f49623afba0bf64af27a724b532c22b46462812ca98988ccbd841500ae89' #current_app.config["GOPHISH_API_KEY"]
    
    def buildHeaders(self):
        headers={"Content-Type": "application/json","Authorization":self.api_key}
        return headers

    def createUser(self,user):
        gp_user={}
        gp_user['role']='user'
        gp_user['username']=user.email
        gp_user['password']='userpassword12!'
        r = requests.post(self.url+'users/', headers=self.buildHeaders(),data=json.dumps(gp_user),verify=False)
    
    def syncUsers(self):
        r = requests.get(self.url+'users/', headers=self.buildHeaders(),verify=False)
        gp_users_dict={}
        for u in r.json():
            gp_users_dict[u['username']]=u

        users = User.query.all() #filter_by(organization_id=g.user.organization_id)
        for user in users:
            if user not in gp_users_dict:
                print('need to create '+user.email)
                self.createUser(user)

def gp():
    x=GophishAPI()
    x.syncUsers()
    return json_response()

        
