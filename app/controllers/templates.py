# Exports to be used in the rest of the module
#
# Copyright (C) 2020 Clovrlabs SL
#
# This program is free software: you can redistribute it
# and/or modify it under the terms of the GNU General Public License as published
# by the Free Software Foundation, either version 3 of the License, or (at your
# option) any later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
# PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# this program. If not, see https://www.gnu.org/licenses/.

from sqlalchemy import desc
from flask import g, request
from flask_json import json_response

from ..core import admin_required, http
from ..models import db, Template

import logging

event_logger = logging.getLogger("event_logger")


@admin_required
def get_templates():
    items = []
    rows = Template.query.filter_by(organization_id=g.user.organization_id).order_by(
        desc(Template.id)
    )
    for row in rows:
        items.append({"id": row.id, "name": row.name, "text": row.text})
    return json_response(items=items)


@admin_required
def get_template(template_id):
    template = Template.query.filter_by(
        organization_id=g.user.organization_id, id=template_id
    ).first()
    if template is None:
        return http.error(http.RECORD_NOT_FOUND)

    return json_response(
        item={"id": template_id, "name": template.name, "text": template.text}
    )


@admin_required
def create_template():
    args = request.json
    name = args.get("name", "").strip()
    text = args.get("text", "").strip()

    if not name or not text:
        return http.error(http.BAD_REQUEST)

    template = Template.query.filter_by(
        name=name, organization_id=g.user.organization_id
    ).one_or_none()
    if template:
        return http.error(http.DUPLICATE_RECORD)

    template = Template()
    template.organization_id = g.user.organization_id
    template.name = name
    template.text = text
    db.session.add(template)
    db.session.commit()

    event_logger.info("template {} created".format(name))
    return json_response(id=template.id)


@admin_required
def update_template(template_id):
    args = request.json
    name = args.get("name", "").strip()
    text = args.get("text", "").strip()

    if not name or not text:
        return http.error(http.BAD_REQUEST)

    template = (
        Template()
        .query.filter_by(organization_id=g.user.organization_id, id=template_id)
        .one_or_none()
    )
    if template is None:
        return http.error(http.RECORD_NOT_FOUND)

    template1 = (
        Template()
        .query.filter_by(organization_id=g.user.organization_id, name=name)
        .one_or_none()
    )
    if template1 and template1.id != template.id:
        return http.error(http.DUPLICATE_RECORD)

    template.name = name
    template.text = text
    db.session.commit()

    event_logger.info("template {} updated".format(name))
    return json_response()


@admin_required
def delete_template(template_id):
    template = Template.query.filter_by(
        organization_id=g.user.organization_id, id=template_id
    ).first()
    if template is None:
        return http.error(http.RECORD_NOT_FOUND)
    template_name = template.name
    db.session.delete(template)
    db.session.commit()

    event_logger.info("template {} deleted".format(template_name))
    return json_response()
