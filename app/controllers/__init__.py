# Exports to be used in the rest of the module
#
# Copyright (C) 2020 Clovrlabs SL
#
# This program is free software: you can redistribute it
# and/or modify it under the terms of the GNU General Public License as published
# by the Free Software Foundation, either version 3 of the License, or (at your
# option) any later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
# PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# this program. If not, see https://www.gnu.org/licenses/.

# TODO: use `*` instead (for example: from .domains import *)
from .auth import login
from .admin import make_admin
from .domains import (
    create_domain,
    delete_domain,
    get_domain,
    get_domains,
    update_domain,
    get_tags_by_domain,
    update_tags_for_domain,
)
from .misc import check_url, get_api_key, new_api_key, remove_account
from .users import (
    get_user,
    get_users,
    invite_user,
    delete_user,
    update_users,
    import_user,
)
from .tags import (
    get_tags,
    get_domains_by_tag,
    get_tag,
    create_tag,
    edit_tag,
    delete_tag,
)
from .templates import (
    get_templates,
    get_template,
    create_template,
    update_template,
    delete_template,
)
from .organization import get_credentials, update_credentials

from .importer import get_route53_zones, get_route53_records

from .analytics import post_analytics

from .registration import register_phishing_email, confirm_phishing_email
