#!/usr/bin/env python
# -*- coding: utf-8 -*-

from flask import current_app, request, g
from flask_json import json_response, JsonError

from ..core import admin_required, http
from ..models import Organization, Credential, db

import logging

event_logger = logging.getLogger("event_logger")


@admin_required
def get_credentials():
    organization = Organization.query.filter_by(id=g.user.organization_id).one_or_none()
    if organization is None:
        raise JsonError(
            status_=500, error="unknown organization for the current admin user"
        )
    if organization.credentials is not None:
        event_logger.info(
            "user {} ({}) requested credentials for organization {}".format(
                g.user.name, g.user.id, organization.domain
            )
        )
        return json_response(credentials=organization.credentials.as_dict())
    return json_response(status_=404, error="no credentials found")


@admin_required
def update_credentials():
    organization = Organization.query.filter_by(id=g.user.organization_id).one_or_none()
    if organization is None:
        raise JsonError(
            status_=500, error="unknown organization for the current admin user"
        )
    data = request.get_json(force=True, silent=True, cache=True)
    if data is None:
        return http.error(http.MISSING_DATA)
    if "credentials" not in data or not isinstance(data["credentials"], dict):
        return http.error(http.BAD_FORMAT)
    if organization.credentials is None:
        organization.credentials = Credential()
    organization.credentials.from_dict(data["credentials"])
    db.session.add(organization)
    db.session.commit()
    return json_response(status_=200, result="credentials updated")
