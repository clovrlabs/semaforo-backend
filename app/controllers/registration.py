#!/usr/bin/env python
# -*- coding: utf-8 -*-

from flask import current_app, request, g, redirect
from flask_json import json_response, JsonError

from ..models import RegistrationState, Registration, db
from ..core import http, send_email

import logging

event_logger = logging.getLogger("event_logger")


def register_phishing_email():
    """This route expects a dict with the email to be registered for
    phishing in the email field, like

    curl -X POST  http://..../v1/phishing/register -d '{"email": "some@email.com"}'
    """
    data = request.get_json(force=True, silent=True, cache=True)
    if data is None:
        return http.error(http.MISSING_DATA)
    if "email" not in data or not isinstance(data, dict):
        return http.error(http.BAD_FORMAT)
    
        
    # TODO: validate provided email address
    registration = Registration.new(data["email"], data.get("cid"), data.get("phone", None))
    # check if the email is already registered and error out if that is the case
    if (
        db.session.query(Registration)
        .filter(Registration.email == data["email"])
        .one_or_none()
        is not None
    ):
        registration = (
        db.session.query(Registration).filter(Registration.email == data['email']).first()
    )
        if not registration.registration_count:
            registration.registration_count = 1
        else:
            registration.registration_count += 1 
        db.session.add(registration)
        db.session.commit()
        return json_response(status_=200, result="email already registered. Count increased")

    config = current_app.config
    if data.get('phone'):
        try:
            registration.send_email(sms=True)
        except Exception as e:
            return http.error(http.INTERNAL_SERVER_ERROR, str(e))

    else: 
        try:
            registration.send_email()
        except Exception as e:
            return http.error(http.INTERNAL_SERVER_ERROR, str(e))
        


    db.session.add(registration)
    db.session.commit()
    return json_response(status_=200, result="email registered")


def confirm_phishing_email():
    """This route expects the token, created in the register_phishing_email step,
    to be passed as a GET argment, like

    http://..../v1/phishing/confirm?token=X3paxgbQjm0ASGJR1IuEnhCLFf5TtZWv
    """
    config = current_app.config
    token = request.args.get("token")
    if token is None:
        raise JsonError(status_=400, error="token missing")
    registration = (
        db.session.query(Registration).filter(Registration.token == token).one_or_none()
    )
    if registration is None:
        raise JsonError(status_=400, error="invalid token")
    if registration.state != RegistrationState.REGISTERED:
        # Increase registration count
        if not registration.registration_count:
            registration.registration_count = 1
        else:
            registration.registration_count += 1 
            
        return redirect("{}/email-confirmation/?already_registered=1".format(config['WEB_SITE']))
    
    if registration.phone:
        registration.send_sms()
        
    registration.confirm()
    db.session.add(registration)
    db.session.commit()
    if registration.phone:
        return redirect("{}/sms-confirmation/?cid={}".format(config["WEB_SITE"], registration.cid))
    return redirect("{}/email-confirmation/".format(config["WEB_SITE"]))
