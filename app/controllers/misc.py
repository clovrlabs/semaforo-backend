# Miscellaneous utility functions and routes that are used across the code
#
# Copyright (C) 2020 Clovrlabs SL
#
# This program is free software: you can redistribute it
# and/or modify it under the terms of the GNU General Public License as published
# by the Free Software Foundation, either version 3 of the License, or (at your
# option) any later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
# PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# this program. If not, see https://www.gnu.org/licenses/.
from datetime import datetime
from secrets import token_urlsafe

from flask import request, g, current_app
from flask_json import json_response

from ..core import http, build_api_key, admin_required, get_fqdn, get_top_domain
from ..models import User, Domain, db

import logging

event_logger = logging.getLogger("event_logger")


def check_url():
    args = request.args
    token = args.get("token")
    url = args.get("url")

    if not url:
        return http.error(http.BAD_REQUEST, "Missing parameters")

    # user without api key or anonymous user, check against the "default" org
    if token is None or not token:
        url = get_fqdn(url)
        event_logger.info("called public check url for {}".format(url[:3]))
        if url in current_app.config["DEFAULT_WHITELIST"] or current_app.config[
            "TOP_1M"
        ].in_top(get_top_domain(url)):
            return json_response(listed=True)
        else:
            return json_response(listed=False)

    user = User.query.filter_by(token=token).one_or_none()
    # authenticated user, check against its own organization
    if user is not None:
        event_logger.info(
            "called check url by {} ({}) for {}".format(user.name, user.id, url[:3])
        )
        # update the user's last API usage
        # TODO: abstract this so that more field can be checked
        user.last_api_access = datetime.now()
        db.session.add(user)
        db.session.commit()

        domain = Domain.query.filter_by(
            url=get_fqdn(url), organization_id=user.organization.id
        ).first()
        # TODO: replace is None by 'lazy evaluation'
        if domain is None:
            return json_response(listed=False)

    return json_response(listed=False)


def get_api_key():
    api_key = build_api_key(g.user.token)
    try:
        api_key = api_key.decode("ascii")
    except:
        pass
    return json_response(api_key=api_key)


def new_api_key():
    g.user.token = token_urlsafe()
    db.session.commit()

    event_logger.info("api key regenerated for user {}".format(g.user.name))
    return json_response(api_key=build_api_key(g.user.token))


@admin_required
def remove_account():
    db.session.delete(g.user.organization)
    db.session.commit()
    event_logger.info("account deleted for {}".format(g.user.organization))
    return json_response(text="Account deleted")
