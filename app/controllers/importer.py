# Domain import  handling routes
#
# Copyright (C) 2020 Clovrlabs SL
#
# This program is free software: you can redistribute it
# and/or modify it under the terms of the GNU General Public License as published
# by the Free Software Foundation, either version 3 of the License, or (at your
# option) any later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
# PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# this program. If not, see https://www.gnu.org/licenses/.

from flask import request, g
from flask_json import json_response, JsonError
from sqlalchemy import desc

from ..core import admin_required, http
from ..core.route53 import Route53Api
from ..models import Domain, db, Organization

import logging

event_logger = logging.getLogger("event_logger")


@admin_required
def get_route53_zones():
    """See route53.py under app/core/route53.py for details, this route returns
    a list of zones in the format
    {
        "id": the zone id as seen by aws, string,
        "name": the zone, for example "clovrlabs.com",
        "private": boolean set to true if the zone is internal
    }
    """
    org = Organization.query.filter_by(id=g.user.organization_id).one_or_none()
    if org.credentials is None:
        raise JsonError(status_=404, error="no credentials set for this organization")
    if (
        not org.credentials.route53_access_key
        or not org.credentials.route53_access_secret
    ):
        raise JsonError(
            status_=404, error="no Route53 configured for this organization"
        )
    try:
        c = Route53Api(
            org.credentials.route53_access_key, org.credentials.route53_access_secret
        )
    except ValueError as e:
        raise JsonError(status_=500, error=str(e))
    return json_response(zones=c._get_hosted_zones())


@admin_required
def get_route53_records(zone_id):
    """See route53.py under app/core/route53.py for details, this route receives
    a zone id in aws format and returns all the records that are registered in it as
    a list in the format
    {
        "name": for example "test" for "test.clovrlabs.com",
        "type": A, AAAA, CNAME etc,
        "ttl": the record's time to live,
        "records": [list of records for this name]
    }
    Please note the zone_id must NOT contain the /hostedzone/ part that is usually added
    by aws
    """
    org = Organization.query.filter_by(id=g.user.organization_id).one_or_none()
    if org.credentials is None:
        raise JsonError(status_=404, error="no credentials set for this organization")
    if (
        not org.credentials.route53_access_key
        or not org.credentials.route53_access_secret
    ):
        raise JsonError(
            status_=404, error="no Route53 configured for this organization"
        )
    try:
        c = Route53Api(
            org.credentials.route53_access_key, org.credentials.route53_access_secret
        )
    except Exception as e:
        raise JsonError(status_=500, error=str(e))

    try:
        return json_response(records=c._get_records(zone_id))
    except ValueError as e:
        raise JsonError(status_=404, error=str(e))
