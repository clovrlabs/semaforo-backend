#!/usr/bin/env python
# -*- coding: utf-8 -*-

from flask import current_app, request, g
from flask_json import json_response, JsonError

from ..core import http
from ..models import Analytics, db

import logging

event_logger = logging.getLogger("event_logger")


def post_analytics():
    data = request.get_json(force=True, silent=True, cache=True)
    if not data or not isinstance(data, dict) or "cid" not in data or "eid" not in data:
        raise JsonError(
            status_=400, error="malformed request, missing data or parameters"
        )
    try:
        record = Analytics.from_data(data)
    except KeyError:
        raise JsonError(status_=400, error="missing parameters")
    except ValueError as e:
        raise JsonError(status_=400, error=str(e))
    except Exception as e:
        raise JsonError(status_=500, error="unhandled: {}".format(str(e)))
    db.session.add(record)
    db.session.commit()
    return json_response(status_=201, id=record.id)
