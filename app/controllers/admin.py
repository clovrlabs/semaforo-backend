# Admin related management routes
#
# Copyright (C) 2020 Clovrlabs SL
#
# This program is free software: you can redistribute it
# and/or modify it under the terms of the GNU General Public License as published
# by the Free Software Foundation, either version 3 of the License, or (at your
# option) any later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
# PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# this program. If not, see https://www.gnu.org/licenses/.

from flask import current_app, request, g
from flask_json import json_response, JsonError

from ..core import gsuite, admin_required, http
from ..models import db, User

import logging

event_logger = logging.getLogger("event_logger")


@admin_required
def make_admin(user_id):
    user = User.query.filter_by(id=user_id).one_or_none()
    if user is None:
        raise JsonError(status_=404, error="malformed user id or no such user found")

    if request.method == "POST":
        user.admin = True
    if request.method == "DELETE":
        user.admin = False
    db.session.add(user)
    db.session.commit()
    event_logger.info("user {} ({}) has been made admin".format(user.name, user.id))
    return json_response(result="user {} made admin".format(user.id))
