# Authentication routes and user handling
#
# Copyright (C) 2020 Clovrlabs SL
#
# This program is free software: you can redistribute it
# and/or modify it under the terms of the GNU General Public License as published
# by the Free Software Foundation, either version 3 of the License, or (at your
# option) any later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
# PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# this program. If not, see https://www.gnu.org/licenses/.

from secrets import token_urlsafe
import jwt
from flask import current_app, request, session
from flask_json import json_response

from ..core import gsuite
from ..core import verify_token, get_email_domain, http
from ..models import User, Organization, db, Template

import logging

event_logger = logging.getLogger("event_logger")


def login():
    # TODO: check required fields
    args = request.json
    google_id_token = args["id_token"]
    google_access_token = args["access_token"]

    try:
        info = verify_token(current_app.config["GOOGLE_CLIENT_ID"], google_id_token)
        session["userid"] = info["sub"]
    except ValueError as err:
        return http.error(http.UNAUTHORIZED, str(err))

    data = gsuite.get_user(google_access_token, info["email"])

    # searches or creates an organization
    domain = get_email_domain(info["email"])
    org = Organization.query.filter_by(domain=domain).first()

    # updates or creates a new user
    user = User.query.filter_by(email=info["email"]).first()
    if not user:
        user = User()

        if not org:
            if not data.get("isAdmin", False):
                return http.error(http.NOT_ADMIN)
            org = Organization()
            org.domain = domain
            db.session.add(org)
            db.session.commit()
            # add default template
            with open("default_template.txt", "r") as myfile:
                dt = myfile.readlines()
            template = Template()
            template.organization_id = org.id
            template.name = "Default Invite"
            template.text = "".join(dt)
            db.session.add(template)
            db.session.commit()

    user.google_admin = data.get("isAdmin", False)
    if data.get("isAdmin", False):
        user.admin = True
    else:
        user.admin = False
    user.token = user.token or token_urlsafe()
    user.organization_id = org.id
    user.google_id = info["sub"]
    user.name = info["name"]
    user.email = info["email"]
    db.session.add(user)
    db.session.commit()

    # TODO: `user_id` is not necessary, since `id_token` already contains the property `sub`
    secret = current_app.config["GOOGLE_CLIENT_SECRET"]
    token = jwt.encode(
        {
            "email": user.email,
            "username": user.name,
            "admin": user.admin,
            "google_admin": user.google_admin,
            "domain": org.domain,
            "id_token": google_id_token,
            "token": google_access_token,
        },
        secret,
        algorithm="HS256",
    )
    if hasattr(token, "decode"):
        token = token.decode("utf8")
    event_logger.info("user {} ({}) logged in".format(user.name, user.id))
    return json_response(token=token)
