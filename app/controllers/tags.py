# Tags handling routes
#
# Copyright (C) 2020 Clovrlabs SL
#
# This program is free software: you can redistribute it
# and/or modify it under the terms of the GNU General Public License as published
# by the Free Software Foundation, either version 3 of the License, or (at your
# option) any later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
# PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# this program. If not, see https://www.gnu.org/licenses/.

from flask import request, g
from flask_json import json_response
from sqlalchemy import desc

from ..core import admin_required, http, get_fqdn
from ..models import Tag, db

import logging

event_logger = logging.getLogger("event_logger")


def get_tags():
    items = []
    visible = request.args.get("visible", None)
    q = Tag.query
    if visible is not None:
        q = q.filter_by(visible=visible, organization_id=g.user.organization_id)
    rows = q.all()
    for row in rows:
        items.append({"id": row.id, "name": row.name, "visible": row.visible})
    return json_response(items=items)


def get_domains_by_tag(tag_id):
    tag = Tag.query.filter_by(
        id=tag_id, organization_id=g.user.organization_id
    ).one_or_none()
    if tag is None:
        return http.error(http.RECORD_NOT_FOUND)
    out = []
    for domain in tag.domains:
        out.append(
            {
                "id": domain.id,
                "url": domain.url,
                "tags": [
                    {"id": x.id, "name": x.name, "visible": x.visible}
                    for x in domain.tags
                ],
            }
        )
    return json_response(items=out)


def get_tag(tag_id):
    tag = Tag.query.filter_by(
        id=tag_id, organization_id=g.user.organization_id
    ).one_or_none()
    if tag is None:
        return http.error(http.RECORD_NOT_FOUND)
    return json_response(item={"id": tag.id, "name": tag.name, "visible": tag.visible})


@admin_required
def create_tag():
    data = request.get_json(force=True, silent=True, cache=True)
    if data is None:
        return http.error(http.MISSING_DATA)
    try:
        tag = Tag()
        tag.name = data["name"]
        tag.visible = data["visible"]
        tag.organization_id = g.user.organization_id
        db.session.add(tag)
        db.session.commit()
    except KeyError as e:
        return http.error(http.BAD_FORMAT)
    except Exception as e:
        return json_response(status_=500, error="unhandled error: {}".format(str(e)))
    event_logger.info("tag created: {}".format(tag.name))
    return json_response(status_=201, id=tag.id)


@admin_required
def edit_tag(tag_id):
    data = request.get_json(force=True, silent=True, cache=True)
    if data is None:
        return http.error(http.MISSING_DATA)
    tag = Tag.query.filter_by(
        id=tag_id, organization_id=g.user.organization_id
    ).one_or_none()
    if tag is None:
        return http.error(http.RECORD_NOT_FOUND)
    try:
        tag.name = data["name"]
        tag.visible = data["visible"]
        db.session.add(tag)
        db.session.commit()
    except KeyError as e:
        return http.error(http.MISSING_DATA)
    except Exception as e:
        return json_response(status_=500, error="unhandled error: {}".format(str(e)))
    event_logger.info("tag {} edited: visible {}".format(tag.name, tag.visible))
    return json_response(status_=200, id=tag.id)


@admin_required
def delete_tag(tag_id):
    tag = Tag.query.filter_by(
        id=tag_id, organization_id=g.user.organization_id
    ).one_or_none()
    if tag is None:
        return http.error(http.RECORD_NOT_FOUND)
    tag_name = tag.name
    db.session.delete(tag)
    db.session.commit()
    # must use tag_name to avoid accessing the now-deleted tag object
    event_logger.info("tag {} deleted".format(tag_name))
    return json_response(status_=200, result="deleted")
