# Domains handling routes
#
# Copyright (C) 2020 Clovrlabs SL
#
# This program is free software: you can redistribute it
# and/or modify it under the terms of the GNU General Public License as published
# by the Free Software Foundation, either version 3 of the License, or (at your
# option) any later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
# PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# this program. If not, see https://www.gnu.org/licenses/.

from flask import request, g
from flask_json import json_response
from sqlalchemy import desc

from ..core import admin_required, http, get_fqdn
from ..models import Domain, Tag, db

import logging

event_logger = logging.getLogger("event_logger")


def get_domains():
    # TODO: is there a better way to "serialize" a result?
    items = []
    domains = Domain.query.filter_by(organization_id=g.user.organization_id).all()
    for domain in domains:
        this = {"id": domain.id, "url": domain.url, "tags": []}
        for tag in domain.tags:
            this["tags"].append(
                {
                    "id": tag.id,
                    "name": tag.name,
                    "visible": tag.visible,
                }
            )
        items.append(this)
    return json_response(items=items)


def get_domain(domain_id):
    domain = Domain.query.filter_by(
        organization_id=g.user.organization_id, id=domain_id
    ).first()
    if domain is None:
        return http.error(http.RECORD_NOT_FOUND)
    this = item = {"id": domain_id, "url": domain.url, "tags": []}
    for tag in domain.tags:
        this["tags"].append(
            {
                "id": tag.id,
                "name": tag.name,
                "visible": tag.visible,
            }
        )
    return json_response(item=this)


def get_tags_by_domain(domain_id):
    domain = Domain.query.filter_by(
        organization_id=g.user.organization_id, id=domain_id
    ).one_or_none()
    if domain is None:
        return http.error(http.RECORD_NOT_FOUND)
    items = []
    for tag in domain.tags:
        items.append(
            {
                "id": tag.id,
                "name": tag.name,
                "visible": tag.visible,
            }
        )
    return json_response(status_=200, items=items)


@admin_required
def update_tags_for_domain(domain_id):
    domain = Domain.query.filter_by(
        organization_id=g.user.organization_id, id=domain_id
    ).one_or_none()
    if domain is None:
        return http.error(http.RECORD_NOT_FOUND)
    data = request.get_json(force=True, silent=True, cache=True)
    if data is None:
        return http.error(http.MISSING_DATA)
    if "tag_ids" not in data or not isinstance(data["tag_ids"], list):
        return http.error(http.BAD_FORMAT)
    # clear out the old list of tags
    domain.tags.clear()
    for idx in data["tag_ids"]:
        this_tag = Tag.query.get(idx)
        if this_tag is None:
            return http.error(http.RECORD_NOT_FOUND)
        domain.tags.append(this_tag)
    db.session.add(domain)
    db.session.commit()
    event_logger.info(
        "domain {} tags updated: {}".format(domain.url, ",".join(domain.tags))
    )
    return json_response(status_=200, ids=[x.id for x in domain.tags])


@admin_required
def create_domain():
    args = request.json
    url = args["url"].strip()

    if not url:
        return http.error(http.BAD_REQUEST)

    domain = Domain.query.filter_by(url=url).first()
    if domain:
        return http.error(http.DUPLICATE_RECORD)

    domain = Domain()
    domain.organization_id = g.user.organization_id
    domain.url = get_fqdn(url)
    db.session.add(domain)
    db.session.commit()

    return json_response(id=domain.id)


@admin_required
def update_domain(domain_id):
    args = request.json
    # TODO: (all) replace array access by .get('name', 'default')
    url = args["url"].strip()

    if not url:
        return http.error(http.BAD_REQUEST)

    domain = (
        Domain()
        .query.filter_by(organization_id=g.user.organization_id, id=domain_id)
        .first()
    )
    if domain is None:
        return http.error(http.RECORD_NOT_FOUND)

    domain1 = (
        Domain()
        .query.filter_by(organization_id=g.user.organization_id, url=url)
        .first()
    )
    if domain1 and domain1.id != domain.id:
        return http.error(http.DUPLICATE_RECORD)

    domain.url = get_fqdn(url)
    db.session.commit()

    return json_response()


@admin_required
def delete_domain(domain_id):
    domain = Domain.query.filter_by(
        organization_id=g.user.organization_id, id=domain_id
    ).first()
    if domain is None:
        return http.error(http.RECORD_NOT_FOUND)
    db.session.delete(domain)
    db.session.commit()

    # TODO: don't repeat yourself (create a common `response` function)
    return json_response()
