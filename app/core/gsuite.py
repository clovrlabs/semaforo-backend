# GSuite end-points.
#
# Copyright (C) 2020 Clovrlabs SL
#
# This program is free software: you can redistribute it
# and/or modify it under the terms of the GNU General Public License as published
# by the Free Software Foundation, either version 3 of the License, or (at your
# option) any later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
# PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# this program. If not, see https://www.gnu.org/licenses/.

import requests


def get_user(access_token, user_key):
    """Returns an user:
    https://developers.google.com/admin-sdk/directory/v1/reference/users/get

    Tries to get the private profile of the user, otherwise retrieves the public profile (see ViewType param).
    """
    view_types = ["admin_view", "domain_public"]
    url = f"https://www.googleapis.com/admin/directory/v1/users/{user_key}"

    for view_type in view_types:
        r = requests.get(
            url, params={"access_token": access_token, "viewType": view_type}
        )
        if r.ok:
            return r.json()

    raise ValueError("Wrong user")


def get_users(access_token, **params):
    """Retrieves a paginated list of either deleted users or all users in a domain:
    https://developers.google.com/admin-sdk/directory/v1/reference/users/list
    """
    url = f"https://www.googleapis.com/admin/directory/v1/users"
    res = requests.get(url, params={"access_token": access_token, **params})

    return res.json()


def make_admin(access_token, user_key, **params):
    """Makes a user a super administrator:
    https://developers.google.com/admin-sdk/directory/v1/reference/users/makeAdmin"""
    url = f"https://www.googleapis.com/admin/directory/v1/users/{user_key}/makeAdmin"
    res = requests.get(url, params={"access_token": access_token, **params})

    return res.json()
