# Constants
#
# Copyright (C) 2020 Clovrlabs SL
#
# This program is free software: you can redistribute it
# and/or modify it under the terms of the GNU General Public License as published
# by the Free Software Foundation, either version 3 of the License, or (at your
# option) any later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
# PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# this program. If not, see https://www.gnu.org/licenses/.

BAD_REQUEST = 400
UNAUTHORIZED = 401
FORBIDDEN = 403
NOT_FOUND = 404
INTERNAL_SERVER_ERROR = 500

# Custom errors
RECORD_NOT_FOUND = (NOT_FOUND, "Record not found", "record_not_found")
DUPLICATE_RECORD = (BAD_REQUEST, "Duplicate record", "duplicate_record")
INVALID_EMAIL = (BAD_REQUEST, "Invalid email address", "invalid_email")
MISSING_DATA = (BAD_REQUEST, "Missing data in request", "missing_data")
BAD_FORMAT = (BAD_REQUEST, "Missing field or unexpected format", "missing_field")
NOT_ADMIN = (FORBIDDEN, "You are not allowed to create organizations", "not_admin")
