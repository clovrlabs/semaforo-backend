# Internal error handling and formatting functions
#
# Copyright (C) 2020 Clovrlabs SL
#
# This program is free software: you can redistribute it
# and/or modify it under the terms of the GNU General Public License as published
# by the Free Software Foundation, either version 3 of the License, or (at your
# option) any later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
# PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# this program. If not, see https://www.gnu.org/licenses/.

from flask_json import json_response


# TODO: the message is redundant
def error(status, message=None):
    _status, _message, _code = (
        status if type(status) is tuple else (status, message, None)
    )

    return json_response(
        status_=_status, message=_message, **({"status": _code} if _code else {})
    )
