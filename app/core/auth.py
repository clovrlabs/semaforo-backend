# Authetnication business logic
#
# Copyright (C) 2020 Clovrlabs SL
#
# This program is free software: you can redistribute it
# and/or modify it under the terms of the GNU General Public License as published
# by the Free Software Foundation, either version 3 of the License, or (at your
# option) any later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
# PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# this program. If not, see https://www.gnu.org/licenses/.

from functools import wraps
from urllib.parse import urlparse, urlunparse

import jwt
from flask import current_app, g, url_for
from google.auth.transport import requests
from google.oauth2 import id_token

from app.core import http


def verify_token(google_client_id, google_id_token):
    """Verifies an ID Token issued by Google's OAuth 2.0 authorization server."""
    id_info = id_token.verify_oauth2_token(
        google_id_token, requests.Request(), google_client_id
    )
    if id_info["iss"] not in ["accounts.google.com", "https://accounts.google.com"]:
        raise ValueError("Wrong issuer")
    return id_info


def build_api_key(token):
    api = urlparse(current_app.config["API_URL"])
    url_checker = urlunparse([api.scheme, api.netloc, url_for("check_url"), "", "", ""])

    return jwt.encode({"checker": url_checker, "token": token}, "", "HS256")


def admin_required(f):
    @wraps(f)
    def dec(*args, **kwargs):
        if g.user is None or not g.user.admin:
            return http.error(http.FORBIDDEN, "Admin access only")
        return f(*args, **kwargs)

    return dec
