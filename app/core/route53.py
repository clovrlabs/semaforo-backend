#!/usr/bin/env python
# -*- coding: utf-8 -*-

import boto3
import botocore


class Route53Api(object):
    def __init__(self, access_key, access_secret, debug=False):
        self.access_key = access_key
        self.access_secret = access_secret
        self.debug = debug
        self.client = boto3.client(
            "route53", aws_access_key_id=access_key, aws_secret_access_key=access_secret
        )
        try:
            self.client.list_hosted_zones()
        except Exception as e:
            raise ValueError("error creating route53 client: {}".format(str(e)))

    def _get_hosted_zones(self, public_only=False):
        data = self.client.list_hosted_zones()
        hosted_zones = []
        for hz in data["HostedZones"]:
            if public_only:
                if hz["Config"]["PrivateZone"]:
                    continue
            hosted_zones.append(
                {
                    "id": hz["Id"],
                    "name": hz["Name"],
                    "private": hz["Config"]["PrivateZone"],
                }
            )
        return hosted_zones

    def _get_records(self, zone_id, types=["A", "CNAME", "AAAA"]):
        try:
            data = self.client.list_resource_record_sets(HostedZoneId=zone_id)
        except botocore.errorfactory.NoSuchHostedZone:
            raise ValueError("no such hosted zone {}".format(zone_id))
        records = []
        for record in data["ResourceRecordSets"]:
            records.append(
                {
                    "name": record["Name"],
                    "type": record["Type"],
                    "ttl": record["TTL"],
                    "records": [x["Value"] for x in record["ResourceRecords"]],
                }
            )
        return records
