# Email handling and validation functions
#
# Copyright (C) 2020 Clovrlabs SL
#
# This program is free software: you can redistribute it
# and/or modify it under the terms of the GNU General Public License as published
# by the Free Software Foundation, either version 3 of the License, or (at your
# option) any later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
# PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# this program. If not, see https://www.gnu.org/licenses/.

import requests
import re
import base64
import smtplib
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart
from email.mime.image import MIMEImage

from email.utils import parseaddr
from flask import render_template


def get_email_domain(email):
    _, addr = parseaddr(email)
    pos = email.rfind("@")
    return email[pos + 1:] if pos > -1 else None


def send_email(_from, to, subject, message, smtp_user, smtp_pass):

    template = render_template("registration-email.mjml")
    print(template)
    headers = {'Content-Type': 'text/html'}
    result = requests.post('http://localhost:8888', data=template, headers=headers)
    print(result)
    msg = MIMEMultipart("alternative")
    msg["Subject"] = subject
    msg["From"] = _from
    msg["To"] = to

    # msg.attach(
    #     MIMEText(
    #         "Your client does not support HTML. Please upgrade to an HTML-enable one to read Semaforo Emails",
    #         "plain",
    #     )
    # )
    # extract embedded images
    m = re.findall('base64,([^"]*)', message)
    counter = 1
    for image in m:
        message = re.sub(
            'img src="data([^"]*)"',
            'img src="cid:image' + str(counter) + '"',
            message,
            1,
        )
        image_bytes = base64.b64decode(image)
        msgImage = MIMEImage(image_bytes)
        msgImage.add_header("Content-ID", "<image" + str(counter) + ">")
        msg.attach(msgImage)
        counter = counter + 1

    msg.attach(MIMEText(result.text, "html"))
    try:
        s=smtplib.SMTP_SSL(host="smtp.gmail.com", port=465)
        s.login(smtp_user, smtp_pass)
        s.send_message(msg)
        s.quit()
    except smtplib.SMTPException as err:
        return http.error(http.INTERNAL_SERVER_ERROR, str(err))
