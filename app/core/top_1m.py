"""This module currently requires network access to load the static top 1 million
domain list from Alexa, if network connectivity is not available a static top-1m.csv
file is required."""
import requests
import zipfile
import csv
import io
import pickle
import logging
from urllib.parse import urlparse


ZIP_URL = "http://s3.amazonaws.com/alexa-static/top-1m.csv.zip"


class Top1M(object):
    """This class receives a list of the top 1 million (or any other list in that format),
    extracts the base from the requested"""

    def __init__(self, url=ZIP_URL, autoload=False, debug=False):
        self.logger = logging.getLogger("event_logger")
        self.url = url
        self.top = {}
        self.debug = debug
        if autoload:
            self.__load()

    def _load_from_pickle(self, f="top_1m.pickle"):
        """Internal function to load the file from pickle should the
        s3 download page be unavailable"""
        self.top = pickle.load(open(f, "rb"))
        self.logger.info("loaded top 1m from pickled file")

    def __load(self):
        """Receives a zipped CSV file"""
        resp = requests.get(self.url)
        if resp.status_code != requests.codes.ok:
            raise ValueError(
                "upstream error ({}): {}".format(resp.status_code, resp.text)
            )
        content = io.BytesIO(resp.content)
        zfile = zipfile.ZipFile(content)
        fname = zfile.namelist()[0]
        fdata = io.TextIOWrapper(io.BytesIO(zfile.read(fname)))
        reader = csv.reader(fdata)
        for row in reader:
            self.top[row[1]] = True
        self.logger.info("loaded top 1m from remote URL")

    def init(self):
        try:
            self.__load()
        except:
            try:
                self._load_from_pickle()
            except:
                pass

    def in_top(self, url):
        return self.top.get(url, False)
