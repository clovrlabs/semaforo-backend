# Url handling and validation business logic
#
# Copyright (C) 2020 Clovrlabs SL
#
# This program is free software: you can redistribute it
# and/or modify it under the terms of the GNU General Public License as published
# by the Free Software Foundation, either version 3 of the License, or (at your
# option) any later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
# PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# this program. If not, see https://www.gnu.org/licenses/.

from urllib.parse import urlparse
import tldextract


def get_fqdn(url):
    """Gets the Fully Qualified Domain Name (FQDN), including the schema."""
    items = urlparse(url)
    netloc = items.netloc or items.path.split("/")[0]

    return netloc

def get_top_domain(url):
    return tldextract.extract(url).domain+"."+tldextract.extract(url).suffix
