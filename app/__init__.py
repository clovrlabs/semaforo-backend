# Main init class of the Semaforo Backend
#
# Copyright (C) 2020 Clovrlabs SL
#
# This program is free software: you can redistribute it
# and/or modify it under the terms of the GNU General Public License as published
# by the Free Software Foundation, either version 3 of the License, or (at your
# option) any later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
# PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# this program. If not, see https://www.gnu.org/licenses/.

import os
import yaml
import logging

# TODO: create a config file (production, development, testing)
import jwt
from flask import Flask, current_app, g, request, render_template, session
from flask_cors import CORS
from flask_json import FlaskJSON, json_response
from apscheduler.schedulers.background import BackgroundScheduler
from apscheduler.jobstores.sqlalchemy import SQLAlchemyJobStore
from apscheduler.executors.pool import ThreadPoolExecutor, ProcessPoolExecutor
from urllib.parse import urlparse
from pytz import utc

import app._env
import app.controllers
from app.core import verify_token, http
from app.core.top_1m import Top1M
from app.middlewares import PrefixMiddleware
from app.models import User, db

# TODO: replace `PUBLIC_ENDPOINTS` by a decorator (for example: @public)
PUBLIC_ENDPOINTS = [
    "/index",
    "/login",
    "/auth_url",
    "/auth_login",
    "/check_url",
    "/check-url",
    "/log",
    "/phishing/register",
    "/phishing/confirm",
]

api = Flask(__name__)
api.config.from_envvar("FLASK_CONFIG")
api.wsgi_app = PrefixMiddleware(
    api.wsgi_app, prefix=urlparse(api.config["API_URL"]).path
)

api.secret_key = b'_5#y2L"F4Q8z\n\xec]/'

api.config["SQLALCHEMY_DATABASE_URI"] = "sqlite:///semaforo.db"
api.config["TOP_1M"] = Top1M(autoload=True, debug=api.config["DEBUG"])

# Flask extensions
CORS(api)
FlaskJSON(api)
db.init_app(api)
db.create_all(app=api)

# configure logging
event_logger = logging.getLogger("event_logger")
formatter = logging.Formatter("(%(asctime)s) %(message)s", "%Y-%m-%d %H:%M:%S")
file_handler = logging.FileHandler("semaforo_events.log")
file_handler.setFormatter(formatter)
event_logger.addHandler(file_handler)
event_logger.setLevel(logging.INFO)

# load the default whitelist stuff
api.config["DEFAULT_WHITELIST"] = []
whitelist_dir = api.config["WHITELIST_DIRECTORY"]
if os.path.isdir(whitelist_dir):
    for filename in [
        x for x in os.listdir(whitelist_dir) if x.endswith("yaml") or x.endswith("yml")
    ]:
        try:
            data = yaml.load(
                open("{}/{}".format(whitelist_dir, filename)), Loader=yaml.SafeLoader
            )
            assert isinstance(data, list)
            api.config["DEFAULT_WHITELIST"].extend(data)
            event_logger.info(
                "loaded whitelist from yaml file {}, {} elements added".format(
                    filename, len(data)
                )
            )
        except Exception as e:
            event_logger.error("unable to load file {}: {}".format(filename, str(e)))
else:
    event_logger.info(
        "no whitelist directory found in {}, skipping loading".format(whitelist_dir)
    )


# scheduler stuff
jobstores = {"default": SQLAlchemyJobStore(url="sqlite:///jobs.sqlite")}
executors = {"default": ThreadPoolExecutor(20), "processpool": ProcessPoolExecutor(5)}
job_defaults = {"coalesce": True, "max_instances": 1}
scheduler = BackgroundScheduler(
    jobstores=jobstores, executors=executors, job_defaults=job_defaults, timezone=utc
)
scheduler = BackgroundScheduler()
scheduler.start()
# in order to access this from the code we set it to a global config option
# so that it can be recovered from the current_app.config context
api.config["scheduler_object"] = scheduler
scheduler.add_job(
    lambda: print("scheduler keepalive running"),
    trigger="interval",
    name="keepalive",
    seconds=10,
)


@api.before_request
def before_request():
    if request.method == "OPTIONS" or request.path in PUBLIC_ENDPOINTS:
        return

    # gets the bearer token from the `Authorization` header
    auth = request.headers.get("Authorization")
    suffix = "Bearer "
    if auth is None or not auth.startswith(suffix):
        return http.error(http.UNAUTHORIZED, "Not authorized")
    client_secret = current_app.config["GOOGLE_CLIENT_SECRET"]
    # TODO: verify that the token is well formed
    token = jwt.decode(auth[len(suffix) :], client_secret, algorithms=["HS256"])

    # TODO: Google `id_tokens` expire in one hour (and your can't change it). Implement a mechanism to extend it.
    if "userid" not in session:
        try:
            info = verify_token(
                current_app.config["GOOGLE_CLIENT_ID"], token["id_token"]
            )
            session["userid"] = info["sub"]
        except ValueError as err:
            return http.error(http.UNAUTHORIZED, str(err))

    user = User.query.filter_by(google_id=session["userid"]).one_or_none()
    if user is None:
        return http.error(http.UNAUTHORIZED)

    # makes the token available to the rest of the handlers and in session

    g.access_token = token["token"]
    g.domain = token["domain"]
    g.user = user


@api.route("/", endpoint="index", strict_slashes=False)
def index():
    return render_template("index.html")


# Auth routes (PUBLIC)
api.add_url_rule("/login", "login", controllers.login, methods=["POST"])

# Domain routes (PRIVATE)
api.add_url_rule("/domains", "get_domains", controllers.get_domains, methods=["GET"])
api.add_url_rule(
    "/domains/<int:domain_id>", "get_domain", controllers.get_domain, methods=["GET"]
)
api.add_url_rule(
    "/domains", "create_domain", controllers.create_domain, methods=["POST"]
)
api.add_url_rule(
    "/domains/<int:domain_id>/tags",
    "get_tags_by_domain",
    controllers.get_tags_by_domain,
    methods=["GET"],
)
api.add_url_rule(
    "/domains/<int:domain_id>/tags",
    "update_tags_for_domain",
    controllers.update_tags_for_domain,
    methods=["PATCH"],
)
api.add_url_rule(
    "/domains/<int:domain_id>",
    "update_domain",
    controllers.update_domain,
    methods=["PUT", "PATCH"],
)
api.add_url_rule(
    "/domains/<int:domain_id>",
    "delete_domain",
    controllers.delete_domain,
    methods=["DELETE"],
)

# Tags routes
api.add_url_rule("/tags", "get_tags", controllers.get_tags, methods=["GET"])
api.add_url_rule(
    "/tags/<int:tag_id>/domains",
    "get_domains_by_tag",
    controllers.get_domains_by_tag,
    methods=["GET"],
)
api.add_url_rule("/tags/<int:tag_id>", "get_tag", controllers.get_tag, methods=["GET"])
api.add_url_rule("/tags", "create_tag", controllers.create_tag, methods=["POST"])
api.add_url_rule(
    "/tags/<int:tag_id>", "edit_tag", controllers.edit_tag, methods=["PATCH"]
)
api.add_url_rule(
    "/tags/<int:tag_id>", "delete_tag", controllers.delete_tag, methods=["DELETE"]
)

# import routes
api.add_url_rule(
    "/import/zones", "import_zones", controllers.get_route53_zones, methods=["GET"]
)
api.add_url_rule(
    "/import/records/<zone_id>",
    "import_records",
    controllers.get_route53_records,
    methods=["GET"],
)


# User routes (PRIVATE)
api.add_url_rule("/users", "get_users", controllers.get_users, methods=["GET"])
api.add_url_rule("/users", "update_users", controllers.update_users, methods=["POST"])
api.add_url_rule(
    "/users/import", "import_user", controllers.import_user, methods=["POST"]
)
api.add_url_rule(
    "/users/<int:user_id>", "get_user", controllers.get_user, methods=["GET"]
)
api.add_url_rule(
    "/users/<int:user_id>/invite",
    "invite_user",
    controllers.invite_user,
    methods=["POST"],
)
api.add_url_rule(
    "/users/<int:user_id>", "delete_user", controllers.delete_user, methods=["DELETE"]
)
api.add_url_rule(
    "/users/make_admin/<int:user_id>",
    "make_user_admin",
    controllers.make_admin,
    methods=["POST", "DELETE"],
)

# Template routes (PRIVATE)
api.add_url_rule(
    "/templates", "get_templates", controllers.get_templates, methods=["GET"]
)
api.add_url_rule(
    "/templates/<int:template_id>",
    "get_template",
    controllers.get_template,
    methods=["GET"],
)
api.add_url_rule(
    "/templates", "create_template", controllers.create_template, methods=["POST"]
)
api.add_url_rule(
    "/templates/<int:template_id>",
    "update_template",
    controllers.update_template,
    methods=["PUT", "PATCH"],
)
api.add_url_rule(
    "/templates/<int:template_id>",
    "delete_template",
    controllers.delete_template,
    methods=["DELETE"],
)

# organization routes
api.add_url_rule(
    "/organization/credentials",
    "get_organization_credentials",
    controllers.get_credentials,
    methods=["GET"],
)

api.add_url_rule(
    "/organization/credentials",
    "update_organization_credentials",
    controllers.update_credentials,
    methods=["POST"],
)

# Misc
api.add_url_rule("/check-url", "check_url", controllers.check_url, methods=["GET"])
api.add_url_rule("/api-key", "get_api_key", controllers.get_api_key, methods=["GET"])
api.add_url_rule("/api-key", "new_api_key", controllers.new_api_key, methods=["POST"])
api.add_url_rule(
    "/remove-account", "remove_account", controllers.remove_account, methods=["DELETE"]
)

# phishing
api.add_url_rule(
    "/phishing/register",
    "phishing_register",
    controllers.register_phishing_email,
    methods=["POST"],
)

# Used in emails
api.add_url_rule(
    "/phishing/confirm",
    "phishing_confirm",
    controllers.confirm_phishing_email,
    methods=["GET"],
)


# Analytics routes
api.add_url_rule("/log", "post_analytics", controllers.post_analytics, methods=["POST"])
