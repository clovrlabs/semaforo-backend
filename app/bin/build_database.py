# Internal script to run the database upon system initialization
#
# Copyright (C) 2020 Clovrlabs SL
#
# This program is free software: you can redistribute it
# and/or modify it under the terms of the GNU General Public License as published
# by the Free Software Foundation, either version 3 of the License, or (at your
# option) any later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
# PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# this program. If not, see https://www.gnu.org/licenses/.

import os
import sys

from sqlalchemy import create_engine
from sqlalchemy.exc import SQLAlchemyError
from sqlalchemy_utils import create_database, database_exists, drop_database

from app import api
from app.models import db

os.system("")


def abort(msg=None):
    """Prints an error in red color and exits from the application."""
    if msg:
        print(f"\033[31mFailed!\033[0m")
        print(msg)
    sys.exit(1)


def build_database():
    try:
        print("Building database ... ", end="")
        engine = create_engine(api.config["SQLALCHEMY_DATABASE_URI"])
        if database_exists(engine.url):
            if "--auto" in sys.argv:
                answer = "y"
            else:
                answer = input(
                    "\nThe database already exist and all data will be lost.\nDo you want to continue? [y/N] "
                )
            if answer.lower() in ("y", "yes"):
                drop_database(engine.url)
            else:
                abort()
        create_database(engine.url)
        db.create_all()
        print("Done!")
    except SQLAlchemyError as err:
        abort(err)


with api.app_context():
    build_database()
