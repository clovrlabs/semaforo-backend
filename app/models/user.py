# User business model for ORM access
#
# Copyright (C) 2020 Clovrlabs SL
#
# This program is free software: you can redistribute it
# and/or modify it under the terms of the GNU General Public License as published
# by the Free Software Foundation, either version 3 of the License, or (at your
# option) any later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
# PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# this program. If not, see https://www.gnu.org/licenses/.

from .database import db


class User(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    organization_id = db.Column(
        db.Integer, db.ForeignKey("organization.id"), nullable=False
    )
    email = db.Column(db.String(255), nullable=False)
    email_sent = db.Column(db.DateTime, nullable=True)
    google_admin = db.Column(db.Boolean, nullable=False)
    admin = db.Column(db.Boolean, nullable=False)
    # until the user logs in for the first time, it can be null
    token = db.Column(db.Text, nullable=True)
    google_id = db.Column(db.String(255), unique=True)
    name = db.Column(db.String(255))
    last_api_access = db.Column(db.DateTime, nullable=True)
    organization = db.relationship("Organization")
    __table_args__ = (db.UniqueConstraint("organization_id", "email"),)

    @classmethod
    def from_json(cls, data):
        u = cls()
        u.id = data["id"]
        u.email = data["email"]
        u.admin = False
        u.token = data["token"]
        u.google_id = data["google_id"]
        u.name = data["name"]
        return u
