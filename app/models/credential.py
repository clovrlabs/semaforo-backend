# Credential business model for ORM access
#
# Copyright (C) 2020 Clovrlabs SL
#
# This program is free software: you can redistribute it
# and/or modify it under the terms of the GNU General Public License as published
# by the Free Software Foundation, either version 3 of the License, or (at your
# option) any later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
# PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# this program. If not, see https://www.gnu.org/licenses/.

from .database import db

CENSOR_NUMBER_OF_ASTERISKS = 4


class Credential(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    organization_id = db.Column(db.Integer, db.ForeignKey("organization.id"))
    organization = db.relationship("Organization", back_populates="credentials")

    gophish_url = db.Column(db.String(255), nullable=True)
    gophish_api_key = db.Column(db.String(255), nullable=True)
    route53_access_key = db.Column(db.String(255), nullable=True)
    route53_access_secret = db.Column(db.String(255), nullable=True)

    def __censor(self, string):
        if string is None:
            return string
        return "{}{}".format(CENSOR_NUMBER_OF_ASTERISKS * "*", string[-4:])

    def as_dict(self):
        return {
            "gophish_url": self.gophish_url,
            "gophish_api_key": self.__censor(self.gophish_api_key),
            "route53_access_key": self.__censor(self.route53_access_key),
            "route53_access_secret": self.__censor(self.route53_access_secret),
            "id": self.id,
        }

    def from_dict(self, data):
        for k, v in data.items():
            if not hasattr(self, k):
                raise ValueError("Tried to load an invalid attribute: {}".format(k))
            if CENSOR_NUMBER_OF_ASTERISKS * "*" in v:
                continue
            setattr(self, k, v)
