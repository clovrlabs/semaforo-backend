# Registration business model for ORM access
#
# Copyright (C) 2020 Clovrlabs SL
#
# This program is free software: you can redistribute it
# and/or modify it under the terms of the GNU General Public License as published
# by the Free Software Foundation, either version 3 of the License, or (at your
# option) any later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
# PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# this program. If not, see https://www.gnu.org/licenses/.

import enum
import string
import random
from datetime import datetime

from sqlalchemy import Enum

from .database import db


class RegistrationState(enum.Enum):
    REGISTERED = "email_added"
    COMPLETED = "completed"


class Registration(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    email = db.Column(db.String(255), nullable=False)
    phone = db.Column(db.String(255), nullable=True)

    cid = db.Column(db.String(255), nullable=False)

    registered_at = db.Column(db.DateTime, nullable=True)
    registration_count = db.Column(db.Integer, nullable=True)
    sms_sent = db.Column(db.Boolean, default=False)
    token = db.Column(db.Text, nullable=False)
    state = db.Column(db.Enum(RegistrationState), nullable=False)
    confirmed_at = db.Column(db.DateTime, nullable=True)

    @staticmethod
    def create_token():
        return "".join(random.sample(string.ascii_letters + string.digits, 32))

    @classmethod
    def new(cls, email, cid, phone):
        r = cls()
        r.email = email
        r.cid = cid
        r.phone = phone
        r.token = Registration.create_token()
        r.state = RegistrationState.REGISTERED
        r.registered_at = datetime.now()
        return r

    def send_sms(self):
        if self.sms_sent:
            raise Exception("Sms already sent")

        import nexmo
        client = nexmo.Client(key="d85226cb", secret="485983ba0a3d8e95")
        # XXX: phones needs to be in full format with country code

        response = client.send_message({'from': 'Semaforo', 'to': self.phone,
                                       'text': 'Sms regarding {}. https://bit.ly/3hdDkdw'.format(self.cid)})
        if response.get('messages') and response.get('messages')[0].get('status') == '0':
            self.sms_sent = True

    def confirm(self):
        self.confirmed_at = datetime.now()
        self.state = RegistrationState.COMPLETED

    def send_email(self, sms=False):
        from ..core import http, send_email
        from flask import current_app
        config = current_app.config

        send_email(
            config["SMTP_FROM"],
            self.email,
            "Please confirm your Semaforo Email",
            "Greetings, to confirm your email please follow this link https://app.semaforo.tech/v1/phishing/confirm?token={}".format(
                self.token
            ),
            config["SMTP_USERNAME"],
            config["SMTP_PASSWORD"],
        )
