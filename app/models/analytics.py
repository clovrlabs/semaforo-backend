# Analytics business model for ORM access
#
# Copyright (C) 2020 Clovrlabs SL
#
# This program is free software: you can redistribute it
# and/or modify it under the terms of the GNU General Public License as published
# by the Free Software Foundation, either version 3 of the License, or (at your
# option) any later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
# PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# this program. If not, see https://www.gnu.org/licenses/.

import enum
from datetime import datetime

from sqlalchemy import Enum, DateTime

from .database import db


class EventCode(enum.Enum):
    EURO2020_LANDING = "a"
    GOOGLE_LOGIN_LOADED = "b"
    GOOGLE_LOGIN_EMAIL_ADDED = "c"
    GOOGLE_LOGIN_PASSWORD_ADDED = "d"
    PLACEHOLDER = "e"
    INSTAGRAM_LOGIN_LOADED = "f"
    INSTAGRAM_LOGIN_CREDENTIALS_ADDED = "g"


class Analytics(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    cid = db.Column(db.String(255), nullable=False)
    eid = db.Column(db.Enum(EventCode), nullable=False)
    timestamp = db.Column(DateTime, nullable=False)

    @classmethod
    def from_data(cls, data):
        if not data or not isinstance(data, dict):
            raise ValueError("missing data or not a dict")
        this = cls()
        this.cid = data["cid"]
        eid = data["eid"]
        if eid not in set(item.value for item in EventCode):
            raise ValueError("invalid event id {}".format(eid))
        for item in EventCode:
            if item.value == eid:
                this.eid = item
                break
        this.timestamp = datetime.now()
        return this
