<!-- TODO: improve documentation -->

# Semaforo API

## Requirements

1. Python 3
2. MySQL server

## Installation

Download the repository and change to the `develop` branch: 
```bash
git clone https://gitlab.com/clovrlabs/semaforo-backend
cd semaforo-backend
git checkout develop
```

Copy `.env-example` and `config-example.cfg` to `.env` and `config.cfg` respectively:
```bash
cp .env-example .env
cp config-example.cfg config.cfg
```

And edit the `config.cfg` file. For example:
```
API_URL="https://path.to.server/v1"

# Go to https://console.cloud.google.com/apis/credentials
# Select a project (if it's not already selected)
# and search OAuth 2.0 Client IDs
GOOGLE_CLIENT_ID="<OAuth 2.0 Client ID>"
GOOGLE_CLIENT_SECRET="<OAuth 2.0 Client Secret>"

SQLALCHEMY_DATABASE_URI="mysql://root:<YOUR_ROOT_PASSWORD>@localhost/semaforo"
SQLALCHEMY_TRACK_MODIFICATIONS=False

SMTP_FROM="<EMAIL>"
SMTP_USERNAME="<USERNAME (PROBABLY `SMTP_FROM`)>"
SMTP_PASSWORD="<PASSWORD>"

INVITATION_SUBJECT="Invitation to join Semaforo"
INVITATION_BODY="You have been invited to join the Semaforo project at http://localhost:3000\n\nKind regards\nThe Semaforo team"
```

Execute the following script:
```bash
./INSTALL
```

The previous script creates a [Virtual Environment](https://docs.python.org/3/tutorial/venv.html), install the required libraries and prepares the database.

## Start the server

Activate the virtual environment and start the server:
```bash
source ./venv/bin/activate
flask run
```